import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { BaseService } from "./base.service";

const httpOptions = {
    headers: new HttpHeaders( {
        'Content-Type': 'application/json'
    } )
};

@Injectable()
export class VolleyballService extends BaseService {


    /*private _host: string;
    private _authToken: string;
    private _headers: HttpHeaders;

    constructor( private _http: Http ) {
        super();
    }*/

    constructor( private http: HttpClient ) {
        super();
    }

    getMatchs() {
        httpOptions.headers.append('Access-Control-Allow-Credentials', '*');
        return this.http.get( super.getUrl() + '/api/match/findAllMatchs' );
    }
    
    savePlay(data:any){
        httpOptions.headers.append('Access-Control-Allow-Credentials', '*');
        return this.http.post(super.getUrl() + '/api/match/saveMatch', data);
    }

}