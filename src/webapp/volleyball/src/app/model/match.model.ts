export class Match {
    constructor(
        public id?: number,
        public teamOne?: string,
        public teamTwo?: string,
        public scoreOne?: number,
        public scoreTwo?: number
    ) {
    }
}