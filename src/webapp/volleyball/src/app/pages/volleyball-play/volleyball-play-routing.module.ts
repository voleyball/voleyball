import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VolleyballPlayComponent } from "app/pages/volleyball-play/volleyball-play.component";

const routes: Routes = [
  {
    path: '',
     component: VolleyballPlayComponent,
    data: {
      title: 'partido de volibol'
    },    
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VolleyballPlayRoutingModule { }