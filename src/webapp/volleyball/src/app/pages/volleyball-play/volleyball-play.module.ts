import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from "@angular/common/http";
import { SharedModule } from "app/shared/shared.module";
import { VolleyballPlayRoutingModule } from "app/pages/volleyball-play/volleyball-play-routing.module";
import { VolleyballPlayComponent } from "app/pages/volleyball-play/volleyball-play.component";
import { VolleyballService } from "app/services/volleyball.service";
import { ToastsManager } from "ng2-toastr/ng2-toastr";


@NgModule( {
    imports: [
        CommonModule,
        FormsModule,
        NgbModule.forRoot(),
        VolleyballPlayRoutingModule,
        ReactiveFormsModule,
        SharedModule,
        HttpModule,
        HttpClientModule
    ],
    declarations: [
        VolleyballPlayComponent
    ],
    providers: [VolleyballService],
    exports: [VolleyballPlayComponent]
} )
export class VolleyballPlayModule { }
