import { Component, OnInit } from '@angular/core';
import { VolleyballService } from "app/services/volleyball.service";
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ScrollHelper } from "app/shared/validator/scrollhelper.component";
import { Match } from "app/model/match.model";
import { Router } from "@angular/router";

@Component( {
    selector: 'app-volleyball-play',
    templateUrl: './volleyball-play.component.html',
    styleUrls: ['./volleyball-play.component.scss']
} )
export class VolleyballPlayComponent implements OnInit {

    form: FormGroup;

    
    scoreHistoryTeamOne = []
    scoreHistoryTeamTwo = []
    
    private scrollHelper: ScrollHelper = new ScrollHelper();

    // estados <section>
    dataPanel = 0
    namePanel = 0
    
    marcador=0

    // valores marcadores
    pointTeamOne = 0
    pointTeamTwo = 0
    
    //valores totales
    totalPointsTeamOne = 0
    totalPointsTeamTwo = 0
    
    // servicios equipos
    serviceTeamOne = 0
    serviceTeamTwo = 0

    // set
    setPlay = 1

    nameTeamOne: string
    nameTeamTwo: string

    datasMatch = new Match();

    constructor( private serviceVolleyball: VolleyballService, private _fb: FormBuilder, private _router: Router ) { }

    ngOnInit() {
        this.initValidators();
        

        console.log( "serviendo t1: " + this.serviceTeamOne )
        console.log( "serviendo t2: " + this.serviceTeamTwo )
    }

    

    initValidators() {
        this.form = this._fb.group( {
            teamone: ['', Validators.required],
            teamtwo: ['', Validators.required],
        } );
    }

    getNameTeam() {

        if ( this.form.valid ) {
            console.log( "team1: " + this.nameTeamOne )
            console.log( "team2: " + this.nameTeamTwo )
            this.namePanel = 1;
            this.dataPanel = 1;
        }

    }



    pointCounting( team: string, sevicePlay: number ) {
        if ( team === "t1" && sevicePlay === 0 && this.setPlay <= 5 ) {
            this.serviceTeamOne = 1
            this.serviceTeamTwo = 0
            this.pointTeamOne = this.pointTeamOne + 1;
            if ( this.pointTeamOne == 26 || this.pointTeamTwo == 26 ) {
                //historico
                this.scoreHistoryTeamTwo.push( this.pointTeamTwo )
                this.scoreHistoryTeamOne.push( this.pointTeamOne )
                console.log( "score t2: " + this.scoreHistoryTeamTwo )
                console.log( "score t1: " + this.scoreHistoryTeamOne )
                //sumatoria de puntos
                this.totalPointsTeamOne =  this.totalPointsTeamOne +  this.pointTeamOne-1;
                this.totalPointsTeamTwo = this.totalPointsTeamTwo + this.pointTeamTwo;
                console.log( "total t1: " + this.totalPointsTeamOne )
                console.log( "total t2: " + this.totalPointsTeamTwo )   
                //reinicio de contadores
                this.pointTeamOne = 0
                this.pointTeamTwo = 0
                // aumento set
                this.setPlay = this.setPlay + 1;
                console.log( "set: " + this.setPlay )
                console.log( "fin del conteo con t1" )
                
                this.datasMatch.teamOne = this.nameTeamOne
                this.datasMatch.teamTwo = this.nameTeamTwo
                this.datasMatch.scoreOne = this.totalPointsTeamOne
                this.datasMatch.scoreTwo = this.totalPointsTeamTwo
                
                console.log( "datos: " + JSON.stringify( this.datasMatch ) )
                
                if(this.setPlay > 5){
                    console.log("fin del juego")
                }
                
            }
        }

        if ( team === "t2" && sevicePlay === 0 && this.setPlay <= 5 ) {
            this.serviceTeamOne = 0
            this.serviceTeamTwo = 1
            this.pointTeamTwo = this.pointTeamTwo + 1;
            if ( this.pointTeamTwo == 26 || this.pointTeamOne == 26 ) {
                //historico
                this.scoreHistoryTeamTwo.push( this.pointTeamTwo )
                this.scoreHistoryTeamOne.push( this.pointTeamOne )
                console.log( "score t2: " + this.scoreHistoryTeamTwo )
                console.log( "score t1: " + this.scoreHistoryTeamOne )
                //sumatoria de puntos
                this.totalPointsTeamOne =  this.totalPointsTeamOne +  this.pointTeamOne;
                this.totalPointsTeamTwo = this.totalPointsTeamTwo + this.pointTeamTwo-1;
                console.log( "total t1: " + this.totalPointsTeamOne )
                console.log( "total t2: " + this.totalPointsTeamTwo )                
                //reinicio de contadores
                this.pointTeamOne = 0
                this.pointTeamTwo = 0
                // aumento set
                this.setPlay = this.setPlay + 1;
                console.log( "set: " + this.setPlay )
                console.log( "fin del conteo con t2" )

                this.datasMatch.teamOne = this.nameTeamOne
                this.datasMatch.teamTwo = this.nameTeamTwo
                this.datasMatch.scoreOne = this.totalPointsTeamOne
                this.datasMatch.scoreTwo = this.totalPointsTeamTwo
                
                

                console.log( "datos: " + JSON.stringify( this.datasMatch ) )
                
                
                if(this.setPlay > 5){
                    console.log("fin del juego")
                }

            }
        }
    }
    
    saveMatch(){        
        console.log("datos a guardar: " + JSON.stringify(this.datasMatch))
        this.serviceVolleyball.savePlay(this.datasMatch).subscribe()
        this._router.navigate( ['/listMatch'] );
    }
    
    returnPageList(){
        this._router.navigate( ['/listMatch'] );
    }

}
