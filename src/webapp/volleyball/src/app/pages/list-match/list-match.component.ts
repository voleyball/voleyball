import { Component, OnInit } from '@angular/core';
import { VolleyballService } from "app/services/volleyball.service";

@Component( {
    selector: 'app-list-match',
    templateUrl: './list-match.component.html',
    styleUrls: ['./list-match.component.scss']
} )
export class ListMatchComponent implements OnInit {
    
    rows: any = [];

    constructor( private serviceVolleyball: VolleyballService ) { }

    ngOnInit() {

        this.getRows();
    }


    getRows() {
        this.serviceVolleyball.getMatchs()
            .subscribe( result => {
                this.rows = result;
                console.log( JSON.stringify( this.rows ) )
            } )
    }
}
