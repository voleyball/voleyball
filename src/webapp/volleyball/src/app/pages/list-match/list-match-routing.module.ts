import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VolleyballPlayComponent } from "app/pages/volleyball-play/volleyball-play.component";
import { ListMatchComponent } from "app/pages/list-match/list-match.component";

const routes: Routes = [
  {
    path: '',
     component: ListMatchComponent,
    data: {
      title: 'listado de partifo'
    },    
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListMatchRoutingModule { }