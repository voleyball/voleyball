import {AbstractControl} from '@angular/forms';

export class FormsValidator{
    static verifyblankspaces(c:AbstractControl){
       if(c.value==null) return null
       if(c.value.indexOf(' ')>=0){
           return {sinEspacios:true}
       }
       return null;
    }
    
    static verifyMinNumbers(c:AbstractControl){
         var regexp = new RegExp(/^\d{10,13}$/)
        if(!regexp.test(c.value)){
            return {minmax:true}
        }
         return null
    }
    static verifyPlaca(c:AbstractControl){
        var regexp= new RegExp(/^[A-Z]{3}\d{4}$/)
                if(!regexp.test(c.value)){
                    return {placa:true}
                }
                return null;
    }
    static verifyMaxHundredChacracters(c:AbstractControl){
        var regexp = new RegExp(/^[\s\S]{0,100}$/)
       if(!regexp.test(c.value)){
           return {maxcharacters:true}
       }
        return null
   }
    static verifyMax45Chacracters(c:AbstractControl){
        var regexp = new RegExp(/^[\s\S]{0,45}$/)
       if(!regexp.test(c.value)){
           return {maxcharacters:true}
       }
        return null
   }
    static verifyMaxTenChacracters(c:AbstractControl){
        var regexp = new RegExp(/^$|^\d{10}$/)
       if(!regexp.test(c.value)){
           return {maxdigits:true}
       }
        return null
   }
    static verifyRuc(c:AbstractControl){
        var regexp = new RegExp(/^[\d]{13}$/)
       if(!regexp.test(c.value)){
           return {ruc:true}
       }
        return null
   }
    static verifyEmail(c:AbstractControl){
        var regexp = new RegExp(/^(?:[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,6})?$/)
       if(!regexp.test(c.value)){
           return {email:true}
       }
        return null
   }
}