import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [

    {
        path: '/play', title: 'Nuevo Partido', icon: 'ft-aperture', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
    },
    {
        path: '/listMatch', title: 'Listado de partidos', icon: 'ft-align-justify', class: '', badge: '', badgeClass: '', isExternalLink: false, submenu: []
    }

];
