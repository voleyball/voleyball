CREATE DATABASE volleyball

CREATE TABLE `matchplay` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `team_one` varchar(255) DEFAULT NULL,
  `team_two` varchar(255) DEFAULT NULL,
  `score_one` int(11) DEFAULT NULL,
  `score_two` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;