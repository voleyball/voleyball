package com.example.volleyballTeam.demo.volleyballTeam.service;

import java.util.List;

import com.example.volleyballTeam.demo.volleyballTeam.model.Match;

public interface MatchService {

	void save(Match match);

	List<Match> findAllMatch();

	Match findById(Long id);
	
	void deleteMatch(Long id);
}
