package com.example.volleyballTeam.demo.volleyballTeam.rest;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.volleyballTeam.demo.volleyballTeam.dto.MatchDTO;
import com.example.volleyballTeam.demo.volleyballTeam.model.Match;
import com.example.volleyballTeam.demo.volleyballTeam.service.MatchService;



@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/api/match")
public class MatchController {
	
	private static final Log logger = LogFactory.getLog(MatchController.class);
	  
	@Autowired
	private MatchService matchService;
	
	Match match = null;
	
	@RequestMapping(value="/saveMatch", method = RequestMethod.POST)
	public ResponseEntity<Void> saveMatch(@RequestBody MatchDTO matchDTO){
		
		if(matchDTO.getId() != null){
			match = matchService.findById(matchDTO.getId());
		} else {
			match = new Match();
		}
		
		match.setTeamOne(matchDTO.getTeamOne());
		match.setTeamTwo(matchDTO.getTeamTwo());
		match.setScoreOne(matchDTO.getScoreOne());
		match.setScoreTwo(matchDTO.getScoreTwo());
		
		// seguimiento de datos
    	logger.info("id guardado: "+match.getId());    
    	logger.info("team1 guardado: "+match.getTeamOne());   
    	logger.info("team2 guardado: "+match.getTeamTwo());   
    	logger.info("score1 guardado: "+match.getScoreOne()); 
    	logger.info("score2 guardado: "+match.getScoreTwo());
		
    	matchService.save(match);
    	
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/findAllMatchs", method = RequestMethod.GET)
	public ResponseEntity<List<Match>> query(){
		List<Match> match = matchService.findAllMatch();
		return new ResponseEntity<>(match, HttpStatus.OK);
	}
	
	@RequestMapping(value="/deleteMatch", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteMatch(@RequestParam Long id){
		matchService.deleteMatch(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
}
