package com.example.volleyballTeam.demo.volleyballTeam.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.volleyballTeam.demo.volleyballTeam.model.Match;
import com.example.volleyballTeam.demo.volleyballTeam.repository.MatchRepository;


@Service
public class MatchServiceImplementation implements MatchService{
	
	@Autowired
	MatchRepository matchRepository;

	@Override
	public void save(Match match) {
		// TODO Auto-generated method stub
		matchRepository.save(match);
	}

	@Override
	public List<Match> findAllMatch() {
		// TODO Auto-generated method stub
		return matchRepository.findAllMatchs();
	}

	@Override
	public Match findById(Long id) {
		// TODO Auto-generated method stub
		return matchRepository.findOne(id);
	}

	@Override
	public void deleteMatch(Long id) {
		// TODO Auto-generated method stub
		matchRepository.delete(id);
	}
	

}
