package com.example.volleyballTeam.demo.volleyballTeam.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.volleyballTeam.demo.volleyballTeam.model.Match;


public interface MatchRepository extends JpaRepository<Match, Long> {

	@Query("SELECT new com.example.volleyballTeam.demo.volleyballTeam.model.Match(u.id, u.teamOne, u.teamTwo, u.scoreOne, u.scoreTwo) FROM Match u order by u.id DESC")
	List<Match> findAllMatchs();

}