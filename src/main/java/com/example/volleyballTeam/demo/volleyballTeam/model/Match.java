package com.example.volleyballTeam.demo.volleyballTeam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="matchplay")
public class Match {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JoinColumn(name = "id")
	private Long id;
	
	@JoinColumn(name = "team_one")
	private String teamOne;
	
	@JoinColumn(name = "team_two")
	private String teamTwo;
	
	@JoinColumn(name = "score_one")
	private Integer scoreOne;
	
	@JoinColumn(name = "score_two")
	private Integer scoreTwo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTeamOne() {
		return teamOne;
	}

	public void setTeamOne(String teamOne) {
		this.teamOne = teamOne;
	}

	public String getTeamTwo() {
		return teamTwo;
	}

	public void setTeamTwo(String teamTwo) {
		this.teamTwo = teamTwo;
	}

	public Integer getScoreOne() {
		return scoreOne;
	}

	public void setScoreOne(Integer scoreOne) {
		this.scoreOne = scoreOne;
	}

	public Integer getScoreTwo() {
		return scoreTwo;
	}

	public void setScoreTwo(Integer scoreTwo) {
		this.scoreTwo = scoreTwo;
	}

	public Match() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Match(Long id, String teamOne, String teamTwo, Integer scoreOne, Integer scoreTwo) {
		super();
		this.id = id;
		this.teamOne = teamOne;
		this.teamTwo = teamTwo;
		this.scoreOne = scoreOne;
		this.scoreTwo = scoreTwo;
	}
	
	
	
}
